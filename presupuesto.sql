select
    "c"."nombre" as "categoria",
    "ip"."monto" as "presupuesto",
    COALESCE( abs(sum(m.monto)) , 0) as monto,
    CASE abs(sum(m.monto)) WHEN null THEN 0 else (abs(sum(m.monto)) / ip.monto) end as factor
from
    "categoria" as "c"
    left join "item_presupuesto" as "ip"
    on "c"."id" = "ip"."id_categoria"
    left join "presupuesto" as "p"
    on "p"."id" = "ip"."id_presupuesto"
    left join "movimiento" as "m"
    on "c"."id" = "m"."id_categoria"
    left join "rol" as "r"
    on "r"."id" = "m"."id_rol"
where
    "c"."tipo" = 'egreso'
    and ("p"."id_usuario" = 3
        or "p"."id_usuario" is null)
    and "r"."id_cuenta" in (
        select "cu"."id"
        from "cuenta" as "cu"
            inner join "rol" as "ro"
            on "ro"."id_cuenta" = "cu"."id"
        where "ro"."id_usuario" = 3)
    and "m"."fecha" >= '2016-10-01 00:00:00'
    and "m"."fecha" < '2016-11-01 00:00:00'
    and "ip"."id" is not null
    and "m"."deleted_at" is null
group by
    "categoria",
    "presupuesto"
union
select
    'No presupuestados' as categoria,
    0 as presupuesto,
    COALESCE(abs(sum(m.monto)), 0) as monto,
    CASE abs(sum(m.monto)) WHEN null THEN 0 ELSE 100 END as factor
from
    "categoria" as "c"
    left join "item_presupuesto" as "ip"
    on "c"."id" = "ip"."id_categoria"
    left join "presupuesto" as "p"
    on "p"."id" = "ip"."id_presupuesto"
    left join "movimiento" as "m"
    on "c"."id" = "m"."id_categoria"
    left join "rol" as "r"
    on "r"."id" = "m"."id_rol"
where
    "c"."tipo" = 'egreso'
    and ("p"."id_usuario" = 3
        or "p"."id_usuario" is null)
    and "r"."id_cuenta" in (
        select "cu"."id"
        from "cuenta" as "cu"
            inner join "rol" as "ro"
            on "ro"."id_cuenta" = "cu"."id"
        where "ro"."id_usuario" = 3)
    and "m"."deleted_at" is null
    and "m"."fecha" >= '2016-10-01 00:00:00'
    and "m"."fecha" < '2016-11-01 00:00:00'
    and "ip"."id" is null
group by
    "categoria",
    "presupuesto"
order by
    "factor" desc,
    "presupuesto" desc,
    "monto" desc
