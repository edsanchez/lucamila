var usuarios = {};

$('#frm-registro').on('submit', function(e){
    e.preventDefault();
    usuarios.registrar();
});
$('#frm-invitar').on('submit', function(e){
    e.preventDefault();
    usuarios.invitar();
});
$('#frm-login').on('submit', function(e){
    e.preventDefault();
    usuarios.login();
});

usuarios.login = function(){
    fw.send({
        url: fw.serviceUrl + fw.webPath.login,
        data: {
            correo: $('#txt-correo').val(),
            password: $('#txt-password').val()
        },
        success: function(res){
            if(res.estado === 'true'){
                location.href = fw.webUrl;
            }
        }
    });
}

usuarios.invitar = function(){
    fw.send({
        url: fw.serviceUrl + 'invitar',
        data: {
            correo: $('#txt-correo').val()
        },
        success: function(res){
            if(res.estado == 'true'){
                alert("Invitación enviada");
            }
        }
    });
}

usuarios.registrar = function(){
    fw.send({
        url: fw.serviceUrl + 'registrar',
        data: {
            nombres: $('#txt-nombres').val(),
            apellidos: $('#txt-apellidos').val(),
            correo: $('#txt-correo').val(),
            password: $('#txt-password').val()
        },
        success: function(res){
            if(res.estado == 'true'){
                location.href = fw.webUrl + 'login';
            }
        }
    });
}
usuarios.activar = function(){
    if(usuarios.enlace != -1){
        fw.send({
            url: fw.serviceUrl + 'activar/' + usuarios.enlace,
            data: {
                nombres: $('#txt-nombres').val(),
                apellidos: $('#txt-apellidos').val(),
                correo: $('#txt-correo').val(),
                password: $('#txt-password').val()
            },
            success: function(res){
                var html = "";
                if(res.estado == 'true'){
                    html = "Tu usuario ha sido activado, ingresa tu usuario y contraseña en ";
                    html += "<a href='" + fw.webUrl + "login" + "'>Login</a> para comenzar a usar la app."
                }else{
                    html = "Ha ocurrido el siguiente error al intentar activar tu cuenta: " + res.mensaje;
                }
                $('#msg-activacion').html(html);
            }
        });
    }
}
