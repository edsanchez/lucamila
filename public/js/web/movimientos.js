var movimientos = {};

movimientos.idCategoria = -1;
// $(document).ready(function(){
//     fw.main({
//         load: loadPage
//     })
// });
movimientos.idCuenta = -1;
movimientos.loadPage = function(){
    fw.send({
        type: 'get',
        url: fw.serviceUrl + 'cuentas/' + movimientos.idCuenta,
        success: function(res){
            if(res.estado === 'true'){
                var cuenta = res.data;
                $('#page-header').html(cuenta.nombre + " [<b>S/" + cuenta.saldo_actual + "</b>]");

                for(i in cuenta.dias.data){

                    var dia = cuenta.dias.data[i];
                    var helperClass = dia.monto < 0 ? 'danger' : 'success';
                    var a = moment(dia.fecha);
                    var b = moment(new Date());
                    var diff = b.diff(a, 'day')
                    var html = '<div class="box box-' + helperClass +'">';
                        html += '<div class="box-header with-border">'
                        html += '<h3 class="box-title">'
                        html += diff == 0 ? "Hoy" : (diff == 1 ? 'Ayer' : ( diff > 7 ? a.format("DD-MMM-YYYY") : a.fromNow()) )
                        html += '</h3>'
                        html += '<div class="box-tools pull-right">'
                        html += '<span class="text-' + helperClass + '">'
                        html += '<b>S/ ' + dia.monto + '</b>'
                        html += '</span>'
                        html += '</div>'
                        html += '</div>'
                        html += '<div class="box-body table-responsive no-padding">'
                        html += '<table class="table table-hover">'
                        html += '<tbody id="lst-movimientos-' + i+ '">'
                        html += '</tbody>'
                        html += '</table>'
                        html += '</div>'
                        html += '</div>';
                    $('#infinite-scroll').append(html);
                    llenarTabla({
                        body: 'lst-movimientos-'+i,
                        selectable: false,
                        data: dia.movimientos,
                        metaData: function(movimiento){
                            var helperClass = movimiento.tipo === "ingreso" ? 'success' : 'danger';
                            return "class='text-" + helperClass + " item-movimiento enlace' data-id='" + movimiento.id + "'";
                        },
                        columns: [
                            {
                                metaData: function(movimiento){
                                    return "style='width:1%; white-space:nowrap;'";
                                },
                                fnCalc: function(movimiento){
                                    return "<i class='fa fa-" + movimiento.icon + "'></i>";
                                }
                            },
                            {
                                dataIndex: 'descripcion'
                            },
                            {
                                metaData: function(movimiento){
                                    return "class='text-right'";
                                },
                                fnCalc: function(movimiento){
                                    return "S/ " + movimiento.monto;
                                }
                            }
                        ]
                    });
                }
            }
        }
    })
}
movimientos.selectCategoria = function(element){
    if(movimientos.idCategoria != -1){
        $('*[data-id="' + movimientos.idCategoria + '"]').removeClass('active');
    }
    $(element).addClass('active');
    movimientos.idCategoria = $(element).data('id');
}
movimientos.loadNewMovimiento = function(){
    if(movimientos.idMovimiento == "-1"){
        fw.main({
            load: function(){
                movimientos.loadIngresos();
                movimientos.loadEgresos();
                movimientos.loadTransferencia();
                movimientos.loadDestinos();
            }
        });
    }else{
        fw.main({
            load: function(){
                movimientos.loadMovimiento();
            }
        });
    }
}
movimientos.loadIngresos = function(){
    fw.send({
        url: fw.serviceUrl + 'categoria/ingreso',
        success: function(res){
            if(res.estado === 'true'){
                movimientos.loadCategorias({
                    categorias : res.data,
                    tipo : 'ingresos'
                });
            }
        }
    })
}
movimientos.loadEgresos = function(){
    fw.send({
        url: fw.serviceUrl + 'categoria/egreso',
        success: function(res){
            if(res.estado === 'true'){
                movimientos.loadCategorias({
                    categorias : res.data,
                    tipo : 'egresos'
                });
            }
        }
    });
}
movimientos.loadTransferencia = function(){
    fw.send({
        url: fw.serviceUrl + 'categoria/transferencia',
        success: function(res){
            if(res.estado === 'true'){
                movimientos.idTransferencia = res.data.id;
            }
        }
    });
}
movimientos.loadCategorias = function(options){
    var categorias = options.categorias;
    var cat = '<br><div class="container-fluid"><div class="row">';
    for(i in categorias){
        var categoria = categorias[i];
        cat += '<div class="col-xs-4 col-md-3 col-lg-2">';
        cat += '<div class="categoria text-center ' + categoria.tipo + ' ';
        if(i == 0 && options.tipo == 'ingresos'){
            cat += 'active';
            movimientos.idCategoria = categoria.id;
        }
        cat += '" data-id="' + categoria.id + '" data-tipo="' + categoria.tipo + '">';
        cat += '<i class="fa fa-2x fa-' + categoria.icon + '"></i><br>';
        cat += '<span>' + categoria.nombre + '</span>';
        cat += '</div>';
        cat += '</div>';
    }
    cat += '</div></div>';
    $('#' + options.tipo).html("");
    $('#' + options.tipo).append(cat);
}
movimientos.loadCuentas = function(args){
    var options = $.extend({
        seleccionar: ''
    }, args);
    var html = "";
    $('#cbo-destino').html("");
    for(i in options.cuentas){
        var cuenta = options.cuentas[i];
        html = '<option value="' + cuenta.id + '" '
            +   (options.seleccionar == cuenta.id ? 'selected="selected"' :'')
            +   ' >' + cuenta.nombre + '</option>';
        $('#cbo-destino').append(html);
    }
}
movimientos.loadMovimiento = function(){
    fw.send({
        url: fw.serviceUrl + 'movimiento/info/' + movimientos.idMovimiento,
        success: function(res){
            if(res.estado === true){
                var movimiento = res.data;
                $('#txt-id').val(movimiento.id);
                $('#txt-monto').val(movimiento.monto);
                $('#txt-fecha').val(movimiento.fecha);
                $('#txt-descripcion').val(movimiento.descripcion);
                movimientos.loadCategorias({
                    categorias : movimiento.categorias_ingreso,
                    tipo : 'ingresos'
                });
                movimientos.loadCategorias({
                    categorias : movimiento.categorias_egreso,
                    tipo : 'egresos'
                });

                var categoria = $('div.categoria[data-id="' + movimiento.id_categoria + '"]');
                if(categoria.length === 0){//Indica transferencia
                    $('input').iCheck('check');
                }else{ //Implica movimiento
                    $('input').iCheck('uncheck');
                    movimientos.selectCategoria(categoria);
                    $('#tab-' + movimiento.tipo).tab("show");
                }

                movimientos.idCuenta = movimiento.id_cuenta;
                movimientos.idCategoria = movimiento.id_categoria;
                movimientos.loadCuentas({
                    cuentas : movimiento.cuentas,
                    seleccionar: movimiento.id_cuenta_referencia
                });
                //movimientos.loadDestinos();
            }
        }
    })
}
movimientos.loadDestinos = function(){
    fw.send({
        url: fw.serviceUrl + 'cuentas/cuentasdestino',
        data: {
            'id_cuenta': movimientos.idCuenta
        },
        success: function(res){
            if(res.estado === 'true'){
                movimientos.loadCuentas({cuentas: res.data});

            }
        }
    })
}
movimientos.seleccionaTransferencia = function(){
    $('#div-transferencia').removeClass('hide');
    $('#div-categorias').addClass('hide');
    //asignar categoria = transferencia
    movimientos.idCategoriaBefore = movimientos.idCategoria;
    movimientos.idCategoria = movimientos.idTransferencia;
}
movimientos.seleccionaMovimiento = function(){
    $('#div-categorias').removeClass('hide');
    $('#div-transferencia').addClass('hide');
    //Asignar categoría = ingreso salario
    movimientos.idCategoria = movimientos.idCategoriaBefore;
}

$('#btn-cancelar').on('click', function(e){
    e.preventDefault();
    //Lanzar cuadro de confirmación;
    location.href = location.origin + util.getPath() + 'cuenta/' + movimientos.idCuenta;
});
$('#frm-movimiento').on('submit', function(e){
    e.preventDefault();

    var url = fw.serviceUrl + 'movimiento';
    var type = 'post';

    if(movimientos.idMovimiento != "-1") {//actualizar
        url = fw.serviceUrl + 'movimiento/' + movimientos.idMovimiento;
        type= 'put';
    }
    fw.send({
        url: url,
        type: type,
        data: {
            'id_categoria': movimientos.idCategoria,
            'id_cuenta_destino' : $('#cbo-destino').val(),
            'fecha': $('#txt-fecha').val(),
            'descripcion': $('#txt-descripcion').val(),
            'monto': $('#txt-monto').val(),
            'id_cuenta': movimientos.idCuenta
        },
        success: function(res){
            if(res.estado === 'true'){
                // if(movimientos.idCuenta == '-1'){
                //     movimientos.idCuenta = res.id_cuenta;
                // }
                location.href = fw.webUrl +'cuenta/'+ movimientos.idCuenta;
            }
        }

    });
});
$('#ingresos').on('click', '.categoria', function(e){
    movimientos.selectCategoria(this);
});
$('#egresos').on('click', '.categoria', function(e){
    movimientos.selectCategoria(this);
});
$('#infinite-scroll').on('click', 'tr.item-movimiento', function(e){
    e.preventDefault();
    var movimientoId = $(this).data('id');
    location.href = fw.webUrl + 'movimiento/' + movimientoId;
});
$('input').iCheck({
    checkboxClass: 'icheckbox_square-blue'
});
$('input').on('ifChecked', function(event){
    movimientos.seleccionaTransferencia();
});
$('input').on('ifUnchecked', function(event){
    movimientos.seleccionaMovimiento();
});
$('#btn-editar-cuenta').on('click', function(e){
    e.preventDefault();
    location.href = fw.webUrl + 'cuenta/edit/' + movimientos.idCuenta;
});
