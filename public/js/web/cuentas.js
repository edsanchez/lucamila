var cuentas = {};
cuentas.idCuenta = -1;


cuentas.loadCuentas = function(){
    fw.main({
        load: function(){
            fillTable({
                paginated: true,
                body: 'lst-cuentas',
                type: 'get',
                url: fw.serviceUrl + 'cuentas',
                selectable: true,
                metaData: function(registro){
                    return 'data-id="' + registro.id + '"';
                },
                columns: [
                    { dataIndex: 'nombre' },
                    {
                        fnCalc: function(registro){
                            return "<td style='text-align: right; width:1%; white-space:nowrap;' class='text-success'>"
                                + "<strong>S/ " + registro.saldo_actual + " <i class='fa fa-caret-up'></i></strong>"
                                + "</td>";
                        }
                    }
                ]
            });
            fillTable({
                paginated: false,
                body: 'lst-presupuesto',
                url: fw.serviceUrl + 'presupuesto/control',
                columns: [
                    { dataIndex: 'categoria' },
                    {
                        fnCalc: function(registro){
                            var porcentaje = Number(registro.monto) > Number(registro.presupuesto) ? 100 : registro.factor * 100;
                            var umbral = registro.factor > util.umbral.danger ? 'danger' : (
                                registro.factor > util.umbral.warning ? 'warning' : 'success'
                            );
                            return '<td style="width: 50%">'
                                + '<div class="progress-sm progress active">'
                                + '<div class="progress-bar progress-bar-' + umbral +' progress-bar-striped" role="progressbar" aria-valuenow="'
                                + porcentaje + '" aria-valuemin="0" aria-valuemax="100" style="width: ' + porcentaje + '%">'
                                + '<span class="sr-only">' + porcentaje + '% Complete</span>'
                                + '</div>'
                                + '</div>'
                                + '</td>';
                        }
                    },
                    {
                        fnCalc: function(registro){
                            return '<td class="text-right"><span class="progress-number"><b>' + registro.monto + '/' + registro.presupuesto + '</b></span></td>';
                        }
                    }
                ]
            });
        }
    });
}

cuentas.loadPage = function(){
    fw.main({
        load: function(res){
            if(cuentas.idCuenta != -1){
                cuentas.loadCuenta();
            }
        }
    });
}

cuentas.loadCuenta = function(){
    fw.send({
        type: 'get',
        url: fw.serviceUrl + 'cuentas/' + cuentas.idCuenta,
        success: function(res){
            if(res.estado == 'true'){
                var cuenta = res.data;
                $('#txt-nombre').val(cuenta.nombre);
                $('#txt-saldo-inicial').val(cuenta.saldo_inicial);
                $('#txt-saldo-inicial').attr('readonly', 'readonly');
            }
        }
    });
}
$('#frm-share').on('submit', function(e){
    e.preventDefault();
    fw.send({
        url: fw.serviceUrl + 'cuentas/share/' + cuentas.idCuenta,
        data: {
            correo: $('#txt-correo').val()
        },
        success: function(res){
            if(res.estado == 'true'){
                
            }
        }
    });
});

$('#frm-cuenta').on('submit', function(e){
    e.preventDefault();
    var url = fw.serviceUrl + 'cuentas';
    var type = 'post';

    if(cuentas.idCuenta != -1){
        url = fw.serviceUrl + 'cuentas/' + cuentas.idCuenta;
        type = 'put';
    }
    fw.send({
        type: type,
        url: url,
        data: {
            nombre: $('#txt-nombre').val(),
            saldo_inicial: $('#txt-saldo-inicial').val()
        },
        success: function(res){
            if(res.estado == 'true'){
                location.href = fw.webUrl;
            }
        }
    });
});

$('#lst-cuentas').on('click', 'tr', function(e){
    e.preventDefault();
    var id = $(this).data('id');
    location.href = fw.webUrl + 'cuenta/' + id;
});
