var base_url = 'http://localhost/lucamila-front/public/';
var login_url = location.origin + util.getPath() + 'login';
function isLogged(sets){
    $.ajax({
        url: 'https://lucamilaws.herokuapp.com/islogged',
        type: "POST",
        dataType: 'json',
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        success: function(res){
            console.log(res);
            if(res.estado === 'true'){
                if(sets.fnYes != null)
                    sets.fnYes(res);
            }else{
                if(sets.fnNo != null)
                    sets.fnNo(res);
            }
        }
    });
}

function logIn(user, password, fn){
    $.ajax({
        url: 'https://lucamilaws.herokuapp.com/login',
        type: "POST",
        data: {
            correo: user,
            password: password
        },
        dataType: 'json',
        xhrFields: {
           withCredentials: true
        },
        crossDomain: true,
        success: function(res){
            console.log(res);
            if(fn != null)
                fn(res);
        }
    });
}

function logOut(fn){
    $.ajax({
        url: 'https://lucamilaws.herokuapp.com/logout',
        type: "POST",
        dataType: 'json',
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        success: function(res){
            if(fn != null)
                fn(res);
        }
    });
}
