//Declaración de constantes
var fw = {};
fw.serviceUrl = 'https://lucamilaws.herokuapp.com/';
fw.webUrl = 'http://lucamila.com/';
// fw.serviceUrl = 'https://lucamilaws.herokuapp.com/';
// fw.webUrl = 'http://localhost/lucamila/public/';
fw.servicePath = {
    validarSesion : 'islogged'
};
fw.webPath = {
    login : 'login'
};

//Declaración de funciones
fw.main = function(options){
    var args = $.extend({
        validarSesion: true,
        load: function(){}
    }, options);

    if(args.validarSesion === true){
        fw.send({
            url: fw.serviceUrl + fw.servicePath.validarSesion,
            success: function(res){
                if(res.estado === 'true'){
                    args.load();
                    $('.container-loader').fadeOut(500);
                }else{
                    location.href = fw.webUrl + fw.webPath.login;
                }
            },
            error: function(res){
                location.href = fw.webUrl + fw.webPath.login;
            }
        });
    }else{
        args.load();
        $('.container-loader').fadeOut(500);
    }
}

fw.send = function(options){
    var args = $.extend({
        url: '',
        type: 'post',
        dataType: 'json',
        data: {},
        success: function(res){
            console.log(res);
        },
        error: function(res){
            console.log(res);
        },
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true
    }, options);
    $.ajax(args);
}

fw.loadUserInfo = function(){
    fw.send({
        url: fw.serviceUrl + 'profile/userinfo',
        success: function(res){
            if(res.estado === 'true'){
                var userInfo = res.data;
                if(userInfo.url_foto != null && userInfo.url_foto != '')
                    $('.profile-foto').attr('src', userInfo.url_foto);
                $('.profile-name').html(userInfo.nombres);
            }
        }
    })
}

function fillTable(args){
    var options = $.extend({
        paginated: false,
        selectable: false,
        body: '',
        type: 'post',
        columns: [],
        url: '',
        params: {}
    }, args);

    $.ajax({
        url: options.url,
        type: options.type,
        dataType: 'json',
        data: options.params,
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        success: function(res){
            if(res.estado == 'true'){
                options.data = options.paginated ? res.data.data : res.data;
                llenarTabla(options);
            }
        }
    });
}

function llenarTabla(options){
    var body = $('#' + options.body);

    var lista = '';
    //var datos = options.paginated ? res.data.data : res.data;
    var datos = options.data;
    for(i in datos){
        var registro = datos[i];
        var tr = '<tr '+ (options.metaData == null ? '' : options.metaData(registro))
            + (options.selectable === true ? ' style="cursor:pointer;" ' : '')
            +'>';
        for(j in options.columns){
            var column = options.columns[j];
            tr += '<td ';
            if(column.metaData != null){
                tr += column.metaData(registro);
            }
            tr += ' >';
            if(column.fnCalc != null){
                tr += column.fnCalc(registro);
            }else{
                tr += registro[column.dataIndex];
            }
            tr += '</td>';
        }
        tr += '</tr>';
        $(body).append(tr);
    }
}

var util = {};
util.umbral = {
    warning: 0.8,
    danger: 1
};
util.path = '/lucamila/public/';
util.getPath = function(){
    if(location.host === 'localhost')
        return util.path;
    return '/'
}


$('#btn-logout').on('click', function(e){
    e.preventDefault();
    fw.send({
        url: fw.serviceUrl + 'logout',
        success: function(res){
            location.href = fw.webUrl + 'login';
            if(res.estado == 'true'){
            }
        }
    })
})
