<?php
namespace App\Logica;

use App\Notificacion;

class NotificacionClass{
    public function store($data){
        $notificacion = new Notificacion();
        $notificacion->codigo = $data['codigo'];
        $notificacion->usuario_id = $data['usuario_id'];
        $notificacion->mensaje = $data['mensaje'];
        $notificacion->enlace = $data['enlace'];
        $notificacion->estado = $data['estado'];
        $notificacion->save();
    }
}
