<?php

namespace App\Listeners;

use App\User;
use App\Cartera;
use App\Cuenta;
use App\Logica\NotificacionClass;
use App\Events\AddMovimiento;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotificacionMovimiento
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AddMovimiento  $event
     * @return void
     */
    public function handle(AddMovimiento $event)
    {
        $cuenta = Cuenta::find($event->movimiento->cuenta_id);
        $cuentas = Cuenta::where('cartera_id', '=', $cuenta->cartera_id)
            ->where('usuario_id', '!=', $cuenta->usuario_id)->get();
        $usuario = User::find(\Auth::user()->id);
        $cartera = Cartera::find($cuenta->cartera_id);

        $mensaje = $usuario->nombre." ha agregado un movimiento a la cartera ".$cartera->nombre;

        foreach($cuentas as $cuenta){
            $data = array(
                "codigo" => 10,
                "usuario_id" => $cuenta->usuario_id,
                "mensaje" => $mensaje,
                "enlace" => asset("cartera/".$cartera->id),
                "estado" => "no visto"
            );

            $notificacion_obj = new NotificacionClass();
            $notificacion_obj->store($data);
        }
    }
}
