<?php

namespace App\Listeners;

use App\User;
use App\Cartera;
use App\Logica\NotificacionClass;
use App\Events\ShareCartera;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class Notificacion implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ShareCartera  $event
     * @return void
     */
    public function handle(ShareCartera $event)
    {
        $cuenta = $event->cuenta;
        $usuario = User::find(\Auth::user()->id);
        $cartera = Cartera::find($cuenta->cartera_id);

        $mensaje = $usuario->nombre." ha compartido la cartera ".$cartera->nombre." contigo";

        $data = array(
            "codigo" => 10,
            "usuario_id" => $cuenta->usuario_id,
            "mensaje" => $mensaje,
            "enlace" => asset("cartera/".$cartera->id),
            "estado" => "no visto"
        );

        $notificacion_obj = new NotificacionClass();
        $notificacion_obj->store($data);
    }
}
