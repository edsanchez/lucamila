<?php

namespace App\Events;

use App\User;
use App\Cartera;
use App\Cuenta;
use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Log;

class ShareCartera extends Event implements ShouldBroadcast
{
    use SerializesModels;

    public $cuenta;
    public $mensaje;
    public $enlace;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Cuenta $cuenta)
    {
        $this->cuenta = $cuenta;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        $cartera = Cartera::find($this->cuenta->cartera_id)->nombre;
        $this->mensaje = \Auth::user()->nombre." ha compartido la cartera ".$cartera." contigo";
        $this->enlace = asset("cartera/".$this->cuenta->cartera_id);
        return ['user'.$this->cuenta->usuario_id];
    }

    public function broadcastAs()
    {
        return 'share';
    }
}
