<?php

namespace App\Events;

use App\Events\Event;
use App\Transaccion;
use App\Cartera;
use App\Cuenta;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class AddMovimiento extends Event
{
    use SerializesModels;

    public $movimiento;
    public $mensaje;
    public $enlace;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Transaccion $movimiento)
    {
        $this->movimiento = $movimiento;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        $cuenta = Cuenta::find($this->movimiento->cuenta_id);
        $cuentas = Cuenta::where('cartera_id', '=', $cuenta->cartera_id)->get();
        $cartera = Cartera::find($cuenta->cartera_id)->nombre;
        $this->mensaje = \Auth::user()->nombre." ha agregado un movimiento a la cartera ".$cartera;
        $this->enlace = asset("cartera/".$this->cuenta->cartera_id);

        $usuarios = array();
        foreach($cuentas as $cuenta){
            $usuarios[] = 'user'.$cuenta->usuario_id;
        }
        return [$usuarios];
    }

    public function broadcastAs()
    {
        return 'new-mov';
    }
}
