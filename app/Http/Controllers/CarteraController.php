<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Cartera;
use App\Cuenta;
use App\Categoria;
use Pusher;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class CarteraController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $carteras = \DB::table('cartera as ca')
            ->select('ca.id', 'ca.nombre','ca.saldo_actual')
            ->join('cuenta as cu', 'ca.id', '=', 'cu.cartera_id')
            ->where('cu.usuario_id', '=', \Auth::user()->id)
            ->paginate(10);
        return view('welcome',[
            'carteras' => $carteras
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('gestion-cartera');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $input = $request->all();
            $cartera = new Cartera();
            $cartera->nombre = $input['nombre'];
            $cartera->saldo_inicial = $input['saldo-inicial'];
            $cartera->saldo_actual = $input['saldo-inicial'];
            $cartera->save();

            $cuenta = new Cuenta();
            $cuenta->usuario_id = \Auth::user()->id;
            $cuenta->cartera_id = $cartera->id;
            $cuenta->rol = 'admin';
            $cuenta->save();
            return \Response::json(array(
                    'error' => false,
                    'data' => $cartera->toArray()),
                    200
                );
        }catch(\Exception $e){
            return \Response::json(array(
                    'error' => false,
                    'data' => $cartera->toArray()),
                    500
                );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cartera = Cartera::find($id);
        $dias = \DB::table('transaccion as t')
            ->select('fecha')
            ->selectRaw('SUM(monto) as monto')
            ->join('cuenta as c', 'c.id', '=', 't.cuenta_id')
            ->join('categoria as cat', 'cat.id', '=', 't.categoria_id')
            ->where('c.cartera_id', '=', $id)
            ->groupBy('t.fecha')
            ->orderBy('t.fecha', 'desc')
            ->paginate(5);
        foreach($dias as $dia){
            $dia->movimientos = \DB::table('transaccion as t')
                ->select('t.id', 't.tipo', 't.descripcion', 't.monto', 't.fecha', 'cat.fa_icon as icon', 'cat.nombre as categoria')
                ->join('cuenta as c', 'c.id', '=', 't.cuenta_id')
                ->join('categoria as cat', 'cat.id', '=', 't.categoria_id')
                ->where('c.cartera_id', '=', $id)
                ->where('t.fecha', '=', $dia->fecha)
                ->orderBy('t.fecha', 'desc')
                ->orderBy('t.id', 'desc')
                ->get();
        }

        return view('cartera', [
            "cartera" => $cartera,
            "dias" => $dias
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cartera = Cartera::find($id);

        $usuarios = \DB::table('usuarios as u')
            ->select('u.id','u.nombre', 'u.apellidos', 'u.correo')
            ->join('cuenta as c', 'u.id', '=', 'c.usuario_id')
            ->where('c.cartera_id', '=', $id)
            ->get();
        $tipo = Cuenta::where('usuario_id', '=', \Auth::user()->id)
            ->where('cartera_id', '=', $id)
            ->first()->rol;
        return view('gestion-cartera', [
            'cartera' => $cartera,
            'usuarios' => $usuarios,
            'rol' => $tipo
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cartera = Cartera::find($id);
        $input = $request->all();

        $inicio = $cartera->saldo_inicial;
        $cartera->nombre = $input['nombre'];
        $cartera->saldo_inicial = $input['saldo-inicial'];
        $cartera->saldo_actual = $cartera->saldo_actual - $inicio + $cartera->saldo_inicial;

        $cartera->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
