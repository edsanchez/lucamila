<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Transaccion;
use App\Cartera;
use App\Cuenta;
use App\Categoria;
use Pusher;
use Event;

use Jenssegers\Date\Date;
use App\Events\AddMovimiento;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class MovimientoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($cartera_id)
    {
        $cartera = Cartera::find($cartera_id);
        $categorias_in = Categoria::where('tipo', '=', 'ingreso')->get();
        $categorias_eg = Categoria::where('tipo', '=', 'egreso')->get();
        return view('gestion-movimiento',
            [
                "cartera"       => $cartera,
                "categorias_in" => $categorias_in,
                "categorias_eg" => $categorias_eg
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $input = $request->all();
            $cuenta = Cuenta::where('usuario_id', '=', \Auth::user()->id)
                ->where('cartera_id', '=', $input['cartera-id'])
                ->first();
            $movimiento = new Transaccion();
            $movimiento->tipo = $input['tipo'];
            $movimiento->monto = $input['tipo'] === 'ingreso' ? $input['monto'] : $input['monto'] * -1;
            $movimiento->fecha = $input['fecha'];
            $movimiento->descripcion = $input['descripcion'];
            $movimiento->categoria_id = $input['categoria'];
            $movimiento->cuenta_id = $cuenta->id;
            $movimiento->save();

            $cartera = Cartera::find($input['cartera-id']);
            $cartera->saldo_actual += $movimiento->monto;
            $cartera->save();

            Event::fire(new AddMovimiento($movimiento));

            return \Response::json(array(
                    'error' => false,
                    'data' => $movimiento->toArray()),
                    200
                );
        }catch(\Exception $e){
            return \Response::json(array(
                    'error' => true,
                    'data' => $movimiento->toArray()),
                    500
                );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $movimiento = Transaccion::find($id);
        $cuenta = Cuenta::find($movimiento->cuenta_id);
        $cartera = Cartera::find($cuenta->cartera_id);
        $categorias_in = Categoria::where('tipo', '=', 'ingreso')->get();
        $categorias_eg = Categoria::where('tipo', '=', 'egreso')->get();
        return view('gestion-movimiento',
            [
                "cartera"       => $cartera,
                "movimiento"    => $movimiento,
                "categorias_in" => $categorias_in,
                "categorias_eg" => $categorias_eg
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $input = $request->all();
            $movimiento = Transaccion::find($id);
            $last_monto = $movimiento->monto;
            $movimiento->tipo = $input['tipo'];
            $movimiento->monto = $input['tipo'] === 'ingreso' ? $input['monto'] : $input['monto'] * -1;
            $movimiento->fecha = $input['fecha'];
            $movimiento->descripcion = $input['descripcion'];
            $movimiento->categoria_id = $input['categoria'];
            $movimiento->save();

            $cartera = Cartera::find($input['cartera-id']);
            $cartera->saldo_actual += (($last_monto * -1) + $movimiento->monto);
            $cartera->save();

            return \Response::json(array(
                    'error' => false,
                    'data' => $movimiento->toArray()),
                    200
                );
        }catch(\Exception $e){
            return \Response::json(array(
                    'error' => true,
                    'data' => $movimiento->toArray()),
                    500
                );
        }
    }

    public function get(Request $request, $id){
        $movimiento = Transaccion::find($id);
        return $movimiento;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $movimiento = Transaccion::find($id);
        $cuenta = Cuenta::find($movimiento->cuenta_id);
        $cartera = Cartera::find($cuenta->cartera_id);
        $cartera->saldo_actual -= $movimiento->monto;
        $cartera->save();

        $movimiento->delete();
        return \Response::json(array(
                'error' => false,
                'data' => $movimiento->toArray()),
                200
        );
    }
}
