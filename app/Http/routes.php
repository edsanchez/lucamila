<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Date::setLocale('es');
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', ['as' =>'auth/login', 'uses' => 'Auth\AuthController@postLogin']);
Route::get('auth/logout', 'Auth\AuthController@getLogout');

Route::get('prueba', function(){
    return view('dashboard');
});
Route::get('/', ['middleware' => ['auth'], 'uses' => 'CarteraController@index']);

Route::group(['prefix' => 'cartera', 'middleware' => 'auth'], function(){
    Route::get('nueva', 'CarteraController@create');
    Route::get('edit/{id}', 'CarteraController@edit');
    Route::get('/{id}/movimiento', 'MovimientoController@create');
    Route::get('/{id}', 'CarteraController@show');
    Route::post('save', 'CarteraController@store');
    Route::post('update/{id}', 'CarteraController@update');
    Route::post('share', 'CuentaController@store');
});

Route::group(['prefix' => 'movimiento', 'middleware' => 'auth'], function(){
    Route::get('{id}', 'MovimientoController@edit');
    Route::post('save', 'MovimientoController@store');
    Route::post('update/{id}', 'MovimientoController@update');
    Route::post('delete/{id}', 'MovimientoController@destroy');
});

Route::post('notificaciones/{usuario_id}', ['middleware' => ['auth'], 'uses' => 'NotificacionController@index']);
