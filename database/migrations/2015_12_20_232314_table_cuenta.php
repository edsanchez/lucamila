<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableCuenta extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cuenta', function (Blueprint $table){
            $table->increments('id');
            $table->integer('usuario_id')->unsigned();
            $table->integer('cartera_id')->unsigned();
            $table->enum('rol', ['admin', 'colaborador']);
            $table->timestamps();

            $table->foreign('usuario_id')
                ->references('id')
                ->on('usuarios');
            $table->foreign('cartera_id')
                ->references('id')
                ->on('cartera');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cuenta');
    }
}
