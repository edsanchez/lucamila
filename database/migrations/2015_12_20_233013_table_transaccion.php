<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableTransaccion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaccion', function(Blueprint $table){
            $table->increments('id');
            $table->enum('tipo', ['ingreso', 'egreso']);
            $table->decimal('monto', 10, 2);
            $table->date('fecha');
            $table->string('descripcion');
            $table->integer('categoria_id')->unsigned();
            $table->integer('cuenta_id')->unsigned();
            $table->timestamps();

            $table->foreign('categoria_id')
                ->references('id')
                ->on('categoria');
            $table->foreign('cuenta_id')
                ->references('id')
                ->on('cuenta');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('transaccion');
    }
}
