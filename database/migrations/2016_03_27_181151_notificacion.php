<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Notificacion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notificacion', function(Blueprint $table){
            $table->increments('id');
            $table->integer('codigo');
            $table->integer('usuario_id')->unsigned();
            $table->text('mensaje');
            $table->string('enlace');
            $table->enum('estado', ['visto', 'no visto']);
            $table->timestamps();

            $table->foreign('usuario_id')
                ->references('id')
                ->on('usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('notificacion');
    }
}
