<?php

use Illuminate\Database\Seeder;

class CategoriasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('categoria')->insert(array(
            ['nombre' => 'Salario', 'tipo' => 'ingreso', 'fa_icon' => 'usd',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")],
            ['nombre' => 'Negocios', 'tipo' => 'ingreso', 'fa_icon' => 'briefcase',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")],
            ['nombre' => 'Regalos', 'tipo' => 'ingreso', 'fa_icon' => 'gift',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")],
            ['nombre' => 'Otros', 'tipo' => 'ingreso', 'fa_icon' => 'money',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")],
        ));
        \DB::table('categoria')->insert(array(
            ['nombre' => 'Pasajes', 'tipo' => 'egreso', 'fa_icon' => 'taxi',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")],
            ['nombre' => 'Comida', 'tipo' => 'egreso', 'fa_icon' => 'cutlery',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")],
            ['nombre' => 'Bebidas', 'tipo' => 'egreso', 'fa_icon' => 'glass',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")],
            ['nombre' => 'Compras', 'tipo' => 'egreso', 'fa_icon' => 'shopping-cart',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")],
            ['nombre' => 'Vahículo', 'tipo' => 'egreso', 'fa_icon' => 'car',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")],
            ['nombre' => 'Viajes', 'tipo' => 'egreso', 'fa_icon' => 'plane',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")],
            ['nombre' => 'Familia/Amigos', 'tipo' => 'egreso', 'fa_icon' => 'users',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")],
            ['nombre' => 'Entretenimiento', 'tipo' => 'egreso', 'fa_icon' => 'ticket',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")],
            ['nombre' => 'Electricidad', 'tipo' => 'egreso', 'fa_icon' => 'bolt',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")],
            ['nombre' => 'Agua', 'tipo' => 'egreso', 'fa_icon' => 'tint',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")],
            ['nombre' => 'Hogar', 'tipo' => 'egreso', 'fa_icon' => 'home',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")],
            ['nombre' => 'Ropa', 'tipo' => 'egreso', 'fa_icon' => 'tag',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")],
            ['nombre' => 'Otros', 'tipo' => 'egreso', 'fa_icon' => 'money',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")]
        ));
    }
}
