<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('usuarios')->insert(array(
            'nombre'    => "Edder",
            'apellidos' => "Sánchez Baca",
            'correo'    => "ed.0810@gmail.com",
            'password'  => \Hash::make('lc*edder*7291'),
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ));
        \DB::table('usuarios')->insert(array(
            'nombre'    => "Ana",
            'apellidos' => "Yamunaqué Carranza",
            'correo'    => "analu.926@gmail.com",
            'password'  => \Hash::make('123456'),
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ));
    }
}
