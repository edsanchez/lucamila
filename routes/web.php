<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
// Date::setLocale('es');
Route::get('login', function(){
    return view('login');
});
Route::get('registro', function(){
    return view('registro');
});
Route::get('activar/{enlace}', function($enlace){
    return view('activacion', [
        'enlace' => $enlace
    ]);
});
//Route::post('login', ['as' =>'auth/login', 'uses' => 'Auth\AuthController@postLogin']);
//Route::get('logout', 'Auth\AuthController@getLogout');

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => 'cuenta'], function(){
    Route::get('nueva', function(){
        return view('cuenta.manage');
    });
    Route::get('edit/{id}', function($id){
        return view('cuenta.manage', [
            'id_cuenta' => $id
        ]);
    });
    Route::get('/{id}/movimiento', function($id){
        return view('cuenta.movimiento', [
            'id_cuenta' => $id
        ]);
    });
    Route::get('/{id}', function($id){
        return view('cuenta.show', [
            'id_cuenta' => $id
        ]);
    });

    // Route::post('save', 'CarteraController@store');
    // Route::post('update/{id}', 'CarteraController@update');
    // Route::post('share', 'CuentaController@store');
});

Route::group(['prefix' => 'movimiento'], function(){
    Route::get('/{id}', function($id){
        return view('cuenta.movimiento', [
            'id_movimiento' => $id
        ]);
    });
});
