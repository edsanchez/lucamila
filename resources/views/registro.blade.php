<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Camila | Ingreso</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css')}}">
        <link rel="stylesheet" href="{{ asset('css/AdminLTE.min.css')}}">
        <link rel="stylesheet" href="{{ asset('css/skins/skin-blue-light.min.css')}}">
        <link rel="stylesheet" href="{{ asset('css/lucamila.css')}}">
    </head>
    <body class='hold-transition login-page'>
        <div class="login-box">
            <div class="login-logo">
                <b>Lucamila</b>
            </div>
            <div class="login-box-body">
                <p class="login-box-msg">Ingresa tus datos para registrarte</p>
                {!! Form::open(['id' => 'frm-registro']) !!}
                <div class="form-group has-feedback">
                    {!!
                        Form::text('nombres', null, [
                        "class"         => "form-control",
                        "id"            => "txt-nombres",
                        'placeholder'   => "Juan"])
                        !!}
                    <span class="fa fa-user form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    {!!
                        Form::text('apellidos', null, [
                        "class"         => "form-control",
                        "id"            => "txt-apellidos",
                        'placeholder'   => "Pérez"])
                        !!}
                    <span class="fa fa-user form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    {!!
                        Form::email('correo', null, [
                        "class"         => "form-control",
                        "id"            => "txt-correo",
                        'placeholder'   => "ejemplo@correo.com"])
                        !!}
                    <span class="fa fa-envelope form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    {!!
                        Form::password('password', [
                        "class"         => "form-control",
                        "id"            => "txt-password",
                        "placeholder"   => "Contraseña"
                        ])
                    !!}
                    <span class="fa fa-lock form-control-feedback"></span>
                </div>
                <div id="messages">

                </div>
                {!! Form::submit('Registrarme', ["class" => "btn btn-primary btn-block btn-flat"]) !!}
                {!! Form::close() !!}

            </div>
        </div>
        <script type="text/javascript" src='{{ asset("js/jquery-2.1.4.min.js") }}'></script>
        <script type="text/javascript" src='{{ asset("js/bootstrap.min.js") }}'></script>
        <script src="{{ asset('js/app.min.js')}}"></script>
        <script src="{{ asset('js/fw.js') }}"></script>
        <script src="{{ asset('js/lucamila.js') }}"></script>
        <script src="{{ asset('js/web/usuarios.js') }}"></script>

    </body>
</html>
