<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Camila | Ingreso</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css')}}">
        <link rel="stylesheet" href="{{ asset('css/AdminLTE.min.css')}}">
        <link rel="stylesheet" href="{{ asset('css/skins/skin-blue-light.min.css')}}">
        <link rel="stylesheet" href="{{ asset('css/lucamila.css')}}">
    </head>
    <body class='hold-transition login-page'>
        <div class="login-box">
            <div class="login-logo">
                <b>Lucamila</b>
            </div>
            <div class="login-box-body">
                <p class="login-box-msg">Activación de cuenta</p>
                <p class="login-box-msg" id='msg-activacion'>
                    <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
                    <span class="sr-only">Loading...</span>
                    Espera un momento mientras activamos tu cuenta...
                </p>
            </div>
        </div>
        <script type="text/javascript" src='{{ asset("js/jquery-2.1.4.min.js") }}'></script>
        <script type="text/javascript" src='{{ asset("js/bootstrap.min.js") }}'></script>
        <script src="{{ asset('js/app.min.js')}}"></script>
        <script src="{{ asset('js/fw.js') }}"></script>
        <script src="{{ asset('js/lucamila.js') }}"></script>
        <script src="{{ asset('js/web/usuarios.js') }}"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                usuarios.enlace = '{{ $enlace or (-1) }}';
                usuarios.activar();
            });
        </script>
    </body>
</html>
