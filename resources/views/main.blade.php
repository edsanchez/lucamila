<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Camila | @yield('title')</title>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css')}}">
        <link rel="stylesheet" href="{{ asset('css/AdminLTE.min.css')}}">
        <link rel="stylesheet" href="{{ asset('css/skins/skin-blue.min.css')}}">
        <link rel="stylesheet" href={{ asset('css/icheck/blue.css')}}>
        <link rel="stylesheet" href="{{ asset('css/lucamila.css')}}">
        <link rel="stylesheet" href="{{ asset('css/loader.css') }}">
         @yield('styles')
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="hold-transition skin-blue sidebar-mini sidebar-collapse">
        <div class="wrapper">
            <header class="main-header">
                <a href="#" class="logo">
                    <span class="logo-mini"><b>LC</b></span>
                    <span class="logo-lg"><b>Lucamila</b></span>
                </a>
                <nav class="navbar navbar-static-top" role="navigation">
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                        <span class="sr-only">Toggle navigation</span>
                    </a>
                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <li class="dropdown messages-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-envelope-o"></i>
                                    <span class="label label-success">4</span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="header">You have 4 messages</li>
                                    <li>
                                        <ul class="menu">
                                            <li>
                                                <a href="#">
                                                    <div class="pull-left">
                                                        <img src="{{ asset('img/user2-160x160.jpg') }}" class="img-circle profile-foto" alt="User Image">
                                                    </div>
                                                    <h4>
                                                        Support Team
                                                        <small><i class="fa fa-clock-o"></i> 5 mins</small>
                                                    </h4>
                                                    <p>Why not buy a new awesome theme? and bla bla bla bla bla bla bla</p>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="footer"><a href="#">See All Messages</a></li>
                                </ul>
                            </li>

                            <li class="dropdown notifications-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-bell-o"></i>
                                    <span class="label label-warning">1</span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="header">Tienes 1 Notificación</li>
                                    <li>
                                        <ul class="menu">
                                            <li>
                                                <a href="#">
                                                    <i class="fa fa-users text-aqua"></i> Nuevo movimiento en  and bla bla bla y más bla bla bla bla
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="footer"><a href="#">Ver todo</a></li>
                                </ul>
                            </li>
                            <!-- Tasks Menu -->
                            <li class="dropdown tasks-menu">
                                <!-- Menu Toggle Button -->
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-flag-o"></i>
                                    <span class="label label-danger">9</span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="header">You have 9 tasks</li>
                                    <li>
                                        <!-- Inner menu: contains the tasks -->
                                        <ul class="menu">
                                            <li><!-- Task item -->
                                                <a href="#">
                                                    <!-- Task title and progress text -->
                                                    <h3>
                                                        Design some buttons
                                                        <small class="pull-right">20%</small>
                                                    </h3>
                                                    <!-- The progress bar -->
                                                    <div class="progress xs">
                                                        <!-- Change the css width attribute to simulate progress -->
                                                        <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                                            <span class="sr-only">20% Complete</span>
                                                        </div>
                                                    </div>
                                                </a>
                                            </li>
                                            <!-- end task item -->
                                        </ul>
                                    </li>
                                    <li class="footer">
                                        <a href="#">View all tasks</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <img src="{{ asset('img/user2-160x160.jpg') }}" class="user-image profile-foto" alt="User Image">
                                    <span class="hidden-xs profile-name"></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <!-- The user image in the menu -->
                                    <li class="user-header">
                                        <img src="{{ asset('img/user2-160x160.jpg') }}" class="img-circle profile-foto" alt="User Image">

                                        <p>
                                            <span class='profile-name'></span>  - Web Developer
                                            <small>Miembro desde enero de 2015</small>
                                        </p>
                                    </li>
                                    <!--li class="user-body">
                                        <div class="row">
                                            <div class="col-xs-4 text-center">
                                                <a href="#">Followers</a>
                                            </div>
                                            <div class="col-xs-4 text-center">
                                                <a href="#">Sales</a>
                                            </div>
                                            <div class="col-xs-4 text-center">
                                                <a href="#">Friends</a>
                                            </div>
                                        </div>
                                    </li-->
                                    <li class="user-footer">
                                        <div class="pull-left">
                                            <a href="#" class="btn btn-default btn-flat">
                                                <i class='fa fa-user'></i> Perfil
                                            </a>
                                        </div>
                                        <div class="pull-right">
                                            <a href="#" id='btn-logout' class="btn btn-default btn-flat">
                                                <i class='fa fa-sign-out'></i> Salir
                                            </a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#" data-toggle="control-sidebar"><i class="fa fa-ellipsis-v"></i></a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>
            <aside class="main-sidebar">
                <section class="sidebar">
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="{{ asset('img/user2-160x160.jpg') }}" class="img-circle profile-foto" alt="User Image">
                        </div>
                        <div class="pull-left info">
                            <p class='profile-name'> </p>
                            <a href="#"><i class="fa fa-circle text-success"></i> Conectado</a>
                        </div>
                    </div>
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                            <input type="text" name="q" class="form-control" placeholder="Buscar...">
                            <span class="input-group-btn">
                                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                    </form>
                    <ul class="sidebar-menu">
                        <!--li class="header">HEADER</li-->
                        <li class='active'><a href="{{ asset('/') }}"><i class='fa fa-folder-open-o'></i> <span>Cuentas</span></a></li>
                        <li><a href="#"><i class='fa fa-user'></i> <span>Perfil</span></a></li>
                        <li><a href="#"><i class='fa fa-cog'></i> <span>Ajustes</span></a></li>

                    </ul>
                </section>
            </aside>
            <div class="content-wrapper">
                <section class="content-header">
                    <h1 id='page-header'>
                        @yield('header-1')
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="{{ asset('/') }}"><i class="fa fa-folder-open-o"></i> Inicio</a></li>
                        @yield('breadcrumb')
                    </ol>
                </section>

                <section class="content">
                    @yield('content')
                </section>
            </div>
            <!--footer class="main-footer">
                <div class="pull-right hidden-xs">

                </div>
                <strong>Copyright &copy; 2015 <a href="#">Company</a>.</strong> All rights reserved.
            </footer-->
            <aside class="control-sidebar control-sidebar-dark">
                <div class="tab-content">
                    <h3 class="control-sidebar-heading">Cuenta</h3>
                    <ul class="control-sidebar-menu">
                        <li>
                            <a href="#" data-toggle="modal" data-target="#mdl-invitar" >
                                <i class="menu-icon fa fa-envelope bg-red"></i>

                                <div class="menu-info">
                                    <h4 class="control-sidebar-subheading">Invitar</h4>
                                    <p>Invita a tus amigos y conocidos a usar la app</p>
                                </div>
                            </a>
                        </li>
                        @yield('sidebar-content')
                    </ul>
                </div>
            </aside>
            <!-- /.control-sidebar -->
            <!-- Add the sidebar's background. This div must be placed
            immediately after the control sidebar -->
            <div class="control-sidebar-bg"></div>
        </div>

        <div class="container-loader">
            <div class="loader">
                <!-- <div class="loader"></div> -->
            </div>
        </div>

        <div class="modal fade" id="mdl-invitar" tabindex="-1" role="dialog" aria-labelledby="mdl-invitar-label">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    {!! Form::open(['class' => 'form-horizontal', 'id' => 'frm-invitar']) !!}
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="mdl-invitar-label">Enviar invitación</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="txt-correo" class='col-sm-3 col-md-2 control-label'>Correo</label>
                            <div class="col-sm-10">
                                <input type="email" class='form-control' name="correo" id='txt-correo'>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class='btn btn-primary pull-left'><i class='fa fa-check'></i> Listo</button>
                        <button type="button" class='btn btn-default' data-dismiss='modal'><i class='fa fa-close'></i> Cancelar</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>

        <!-- ./wrapper -->
        <!-- jQuery 2.2.0 -->
        <script src="{{ asset('js/jquery-2.1.4.min.js')}}"></script>
        <!-- Bootstrap 3.3.6 -->
        <script src="{{ asset('js/bootstrap.min.js')}}"></script>
        <script src="{{ asset('js/icheck.min.js')}}"></script>

        <script src="{{ asset('js/moment.js')}}"></script>
        <script type="text/javascript">
            moment.locale("es");
        </script>
        <!-- AdminLTE App -->
        <script src="{{ asset('js/app.min.js')}}"></script>
        <script src="{{ asset('js/fw.js')}}"></script>
        <script src="{{ asset('js/web/usuarios.js') }}"></script>
        <script type="text/javascript">
            fw.loadUserInfo();
        </script>
        <!-- <script src="{{ asset('js/lucamila.js')}}"></script> -->

        @yield('scripts')
    </body>
</html>
