@extends('main')

@section('title', 'Nueva cuenta')

@section('content')
<div class="container-fluid">
    <h1>Nueva cartera</h1>
    {!! Form::open(['class' => 'form-horizontal', 'id' => 'frm-new-cartera']) !!}
    <div class="form-group">
        <label for="txt-nombre" class='control-label col-sm-3 col-md-2'>Nombre</label>
        <div class="col-sm-9 col-md-10">
            <input type="text" class='form-control' name="nombre" placeholder='P.e.: Ahorros'>
        </div>
    </div>
    <div class="form-group">
        <label for="txt-saldo" class='control-label col-sm-3 col-md-2'>Saldo Inicial</label>
        <div class="col-sm-5 col-md-4">
            <div class="input-group">
                <span class="input-group-addon">S/.</span>
                <input type="number" step='any' class='form-control' name="saldo-inicial" placeholder='0.00'>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-9 col-md-10 col-sm-offset-3 col-md-offset-2">
            <button type="submit" class='btn btn-primary'><i class='fa fa-plus'></i> Agregar</button>
            <button type="button" class='btn btn-default' id='btn-cancelar'><i class='fa fa-ban'></i> Cancelar</button>
        </div>
    </div>
    {!! Form::close() !!}
</div>
@stop

@section('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $('#frm-new-cartera').on('submit', function(e){
            e.preventDefault();
            $.ajax({
                url: '{{ asset("cartera/save") }}',
                type: 'post',
                dataType: 'json',
                data: new FormData(this),
                contentType: false,
                processData: false,
                success: function(response){
                    location.href = '{{ asset("/") }}';
                }
            });
        });
    });
</script>
@stop
