<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Camila | @yield('title')</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}" media="screen" title="no title" charset="utf-8">
        <link rel="stylesheet" href="{{ asset('css/sidebar.css') }}" media="screen" title="no title" charset="utf-8">
        <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}" media="screen" title="no title" charset="utf-8">
        <link rel="stylesheet" href="{{ asset('css/main.min.css') }}" media="screen" title="no title" charset="utf-8">
    </head>
    <body>
        <header>
            <nav class='navbar navbar-default navbar-fixed-top'>
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle navbar-button collapsed" id='menu-toggle'
                            aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a href='{{ asset("/") }}' class='navbar-brand'>Camila</a>
                        <div class="nav pull-right">
                            <div class="dropdown pull-right">
                                <a href='#' class="navbar-more" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    <i class='fa fa-ellipsis-v'></i>
                                </a>
                                <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                    <li><a href="{{ asset('cartera/nueva') }}"><i class='fa fa-folder-o'></i> Nueva Cartera</a></li>
                                    @yield('items-menu')
                                    <li role="separator" class="divider"></li>
                                    <li>
                                        <a href="#"><i class='fa fa-user'></i> {{ \Auth::user()->nombre }}</a>
                                    </li>
                                    <li role="separator" class="divider"></li>
                                    <li>
                                        <a href="{{ asset('auth/logout') }}"><i class='fa fa-sign-out'></i> Salir</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="dropdown pull-right">
                                <a href='#' class="navbar-more" id="notifications" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    <i class='fa fa-bell-o'></i>
                                </a>
                                <ul class="dropdown-menu" aria-labelledby="notifications" id='notifications-menu'>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </nav>
        </header>
        <div id='wrapper'>
            <aside id='sidebar-wrapper'>
                <ul class='sidebar-nav'>
                    <li><a href="{{ asset('/') }}"><i class='fa fa-folder-open-o'></i> Cuentas</a></li>
                    <li><a href="#"><i class='fa fa-user'></i> Perfil</a></li>
                    <li><a href="#"><i class='fa fa-cog'></i> Ajustes</a></li>
                </ul>
            </aside>
            <section id='page-content-wrapper' class='@yield("with-flat-btn")'>
                @yield('content')
            </section>
        </div>
        <script type="text/javascript" src='{{ asset("js/jquery-2.1.4.min.js") }}'></script>
        <script src="https://js.pusher.com/3.0/pusher.min.js"></script>
        <script type="text/javascript" src='{{ asset("js/bootstrap.min.js") }}'></script>
        <script type="text/javascript" src='{{ asset("js/main.js") }}'></script>
        <script>
            // Enable pusher logging - don't include this in production
            $(document).ready(function(){
                Pusher.log = function(message) {
                    if (window.console && window.console.log) {
                        window.console.log(message);
                    }
                };
                var pusher = new Pusher('c9d90e80e4a15879bac2', {
                    encrypted: true
                });
                var channel = pusher.subscribe('user{{ \Auth::user()->id}}');
                channel.bind('share', function(data) {
                    addNotification();
                });
                channel.bind('new-mov', function(data) {
                    addNotification();
                });

                loadNotifications();
            });

            function addNotification(data){
                var html = $('#notifications-menu').html();
                html = "<li><a href='" + data.enlace + "'>" + data.mensaje + "</a></li>" + html;
                $('#notifications-menu').html(html);
            }

            function loadNotifications(){
                var formData = new FormData();
                formData.append('_token', '{{ csrf_token() }}');
                $.ajax({
                    url: "{{ asset('notificaciones')}}/{{ \Auth::user()->id}}",
                    type: 'post',
                    data: formData,
                    contentType: false,
                    processData: false,
                    success: function(response){
                        var html = "";
                        for(var i = 0; i < response.length; i++){
                            var notificacion = response[i];
                            html += "<li><a href='" + notificacion.enlace + "'>" + notificacion.mensaje + "</a></li>";
                        }
                        if(response.length == 0)
                            html += "<li>No tienes notificaciones</li>";
                        $('#notifications-menu').html(html);
                    }
                });
            }
        </script>
        @yield('scripts')
    </body>
</html>
