@extends('main')

@section('title', 'Movimiento')
@section('breadcrumb')
    <li><a href='{{asset("cartera/".$cartera->id)}}'>{{ $cartera->nombre }}</a></li>
    <li class="active">Movimiento</li>
@stop
@section('styles')
<link rel="stylesheet" href="{{asset('css/datepicker3.css')}}" media="screen" title="no title" charset="utf-8">
@stop
@section('header-1')
{{ isset($movimiento) ? "Editar movimiento" : "Nuevo movimiento" }}
@stop
@section('content')
<div class="page-with-fab-btn">
    {!! Form::open(['class' => 'form-horizontal', 'id' => 'frm-movimiento']) !!}
    <input type='hidden' name='cartera-id' value='{{ isset($cartera) ? $cartera->id : -1 }}'/>
    <input type='hidden' name='movimiento-id' value='{{ isset($movimiento) ? $movimiento->id : -1 }}'/>
    <div class="form-group">
        <label for="txt-monto" class='col-sm-3 col-md-2 control-label'>Monto</label>
        <div class="col-sm-9 col-md-3">
            <div class="input-group">
                <span class="input-group-addon">S/.</span>
                {!! Form::text('monto', abs( isset($movimiento) ? $movimiento->monto : 0), ["id" => "txt-monto", "class" => 'form-control']) !!}
            </div>
        </div>
    </div>
    <div class='form-group'>
        <label for="txt-fecha" class='col-sm-3 col-md-2 control-label'>Fecha</label>
        <div class="col-sm-9 col-md-3">
            {!! Form::text('fecha', ( isset($movimiento->fecha) ? (new Date($movimiento->fecha))->format('Y/m/d') : Date::now()->format('Y/m/d')), ['class' => 'form-control', 'id' => 'txt-fecha']) !!}
        </div>
    </div>
    <div class="form-group">
        <label for="rb-tipo" class='col-sm-3 col-md-2 control-label'>Tipo</label>
        <div class="col-sm-9 col-md-10">
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="{{ isset($movimiento) ? ($movimiento->tipo === 'ingreso' ? 'active' : '') : 'active'}}"><a href="#ingresos" aria-controls="home" role="tab" data-toggle="tab">Ingreso</a></li>
                <li role="presentation"  class="{{ isset($movimiento) ? ($movimiento->tipo === 'egreso' ? 'active' : '') : ''}}"><a href="#gastos" aria-controls="profile" role="tab" data-toggle="tab">Gasto</a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane {{isset($movimiento) ? ($movimiento->tipo === 'ingreso' ? 'active' : '') : 'active'}}" id="ingresos">
                    <br>
                    <div class="container-fluid">
                        <div class="row">
                        @for($i = 0; $i < count($categorias_in); $i++)
                        <?php $categoria = $categorias_in[$i]; ?>
                            <div class="col-xs-4 col-md-3 col-lg-2">
                                <div class="categoria text-center ingreso {{ isset($movimiento) ? ($categoria->id == $movimiento->categoria_id ? 'active' : '') : ($i == 0 ? 'active' : '') }}" data-id='{{ $categoria->id }}' data-tipo='ingreso'>
                                    <i class='fa fa-2x fa-{{ $categoria["fa_icon"] }}'></i><br>
                                    <span>{{ $categoria->nombre }}</span>
                                </div>
                            </div>
                        @endfor
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane {{isset($movimiento) ? ($movimiento->tipo === 'egreso' ? 'active' : '') : ''}}" id="gastos">
                    <br>
                    <div class="container-fluid">
                        <div class="row">
                        @for($i = 0; $i < count($categorias_eg); $i++)
                        <?php $categoria = $categorias_eg[$i]; ?>
                            <div class="col-xs-4 col-md-3 col-lg-2">
                                <div class="categoria text-center egreso {{ isset($movimiento) ? ($categoria->id == $movimiento->categoria_id ? 'active' : '') : '' }}" data-id='{{ $categoria->id }}' data-tipo='egreso'>
                                    <i class='fa fa-2x fa-{{ $categoria["fa_icon"] }}'></i><br>
                                    <span>{{ $categoria->nombre }}</span>
                                </div>
                            </div>
                        @endfor
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label for="txt-descripcion" class='col-sm-3 col-md-2 control-label'>Descripción</label>
        <div class="col-sm-9 col-md-10">
            {!! Form::text('descripcion', isset($movimiento) ? $movimiento->descripcion : '', ['class' => 'form-control', 'id' => 'txt-descripcion']) !!}
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-9 col-md-10 col-sm-offset-3 col-md-offset-2">
            <button type="submit" class='btn btn-primary'><i class='fa fa-check'></i> Listo</button>
            <button type="button" class='btn btn-default'><i class='fa fa-ban'></i> Cancelar</button>
            @if( isset($movimiento) )
            <button type="button" class='btn btn-danger' id='btn-delete'><i class='fa fa-trash'></i> Eliminar</button>
            @endif
        </div>
    </div>
    {!! Form::close() !!}
    @if(isset($usuarios))
    <h2>Participantes</h2>
    <div class="list-group">
        @foreach($usuarios as $usuario)
        <a href="#" class='list-group-item'>
            @if($usuario->id == \Auth::user()->id)
            Tú
                @if($rol === 'colaborador')
                <button class='btn btn-danger btn-sm'>Abandonar</button>
                @endif
            @else
            {{$usuario->correo}}
                @if($rol === 'admin')
                <button class='btn btn-warning btn-sm'>Retirar</button>
                @endif
            @endif
        </a>
        @endforeach
    </div>
    @endif
</div>
@stop

@section('scripts')
<script type="text/javascript" src='{{ asset("js/jquery.inputmask.js")}}'></script>
<script type="text/javascript" src='{{ asset("js/jquery.inputmask.numeric.extensions.js")}}'></script>
<script type="text/javascript" src='{{ asset("js/jquery.inputmask.date.extensions.js")}}'></script>
<script type="text/javascript" src='{{ asset("js/jquery.inputmask.extensions.js")}}'></script>

<script type="text/javascript" src='{{ asset("js/bootstrap-datepicker.js")}}'></script>
<script type="text/javascript">
/* Incluir en archivo javascript general*/
    $.fn.datepicker.dates['es'] = {
        days: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
        daysShort: ["Dom", "Lun", "Mar", "Mier", "Jue", "Vie", "Sab"],
        daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
        months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
        monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
        today: "Hoy",
        clear: "Limpiar",
        format: "yyyy/mm/dd",
        titleFormat: "MM yyyy", /* Leverages same syntax as 'format' */
        weekStart: 0
    };

    var categoria_id = {{ isset($movimiento) ? $movimiento->categoria_id : 1 }};
    $(document).ready(function(){
        $('#txt-monto').inputmask({alias: 'decimal'});
        $('#txt-fecha').datepicker({
            language: 'es'
        });
        $('#txt-monto').focus();
        $('#frm-movimiento').on('submit', function(e){
            e.preventDefault();
            var formData = new FormData(this);
            formData.append('tipo', $('*[data-id="' + categoria_id + '"]').data('tipo'));
            formData.append('categoria', categoria_id);
            @if( isset($movimiento) )
                var url = "{{ asset('movimiento/update/'.$movimiento->id) }}";
            @else
                var url = "{{asset('movimiento/save') }}";
            @endif
            $.ajax({
                url: url,
                type: 'post',
                dataType: 'json',
                processData: false,
                contentType: false,
                data: formData,
                success: function(response){
                    location.href = '{{ asset("cartera/".$cartera->id)}}'
                }
            });
        });
        @if( isset($movimiento) )
            $('#btn-delete').on('click', function(e){
                e.preventDefault();
                var formData = new FormData();
                formData.append('_token', '{{ csrf_token() }}');
                $.ajax({
                    url: "{{asset('movimiento/delete/'.$movimiento->id)}}",
                    type: 'post',
                    dataType: 'json',
                    processData: false,
                    contentType: false,
                    data: formData,
                    success: function(response){
                        location.href = '{{ asset("cartera/".$cartera->id)}}';
                    }
                });
            });
        @endif
        $('.categoria').on('click', function(e){
            if(categoria_id != -1){
                $('*[data-id="' + categoria_id + '"]').removeClass('active');
            }
            $(this).addClass('active');
            categoria_id = $(this).data('id');
        });
    });
</script>
@stop
