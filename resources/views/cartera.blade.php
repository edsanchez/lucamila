@extends('main')

@section('title', 'Movimientos')

@section('header-1')
    {{$cartera->nombre}} <small>S/. {{ $cartera->saldo_actual }}</small>
@stop
@section('sidebar-content')
    <h3 class="control-sidebar-heading">Cartera</h3>
    <ul class="control-sidebar-menu">
        <li>
            <a href="#" data-toggle="modal" data-target="#mdl-share" >
                <i class="menu-icon fa fa-share-alt bg-red"></i>

                <div class="menu-info">
                    <h4 class="control-sidebar-subheading">Compartir</h4>
                    <p>Agregar miembros a la cartera</p>
                </div>
            </a>
        </li>
        <li>
            <a href="{{ asset('cartera/edit/'.$cartera->id)}}">
                <i class='menu-icon fa fa-pencil bg-red'></i>
                <div class="menu-info">
                    <h4 class='control-sidebar-subheading'>Editar</h4>
                    <p>
                        Modificar los datos de la cartera
                    </p>
                </div>
            </a>
        </li>
    </ul>
@stop
<!--
<li><a href="#" data-toggle="modal" data-target="#mdl-share" ><i class='fa fa-share-alt'></i> Compartir</a></li>
-->
@section('breadcrumb')
    <li class="active">{{ $cartera->nombre }}</li>
@stop
@section('content')
<div class="page-with-fab-btn" id='infinite-scroll'>
    @foreach($dias as $dia)
    <?php
        $date = new Date($dia->fecha);
        $diff = $date->diffInDays(Date::now());

        $helperClass = $dia->monto < 0 ? 'danger' : 'success';
    ?>
    <div class="box box-{{$helperClass}}">
        <div class="box-header with-border">
            <h3 class="box-title">
                {{ $diff == 0 ? "Hoy" : ($diff == 1 ? 'Ayer' : ( $diff > 7 ? $date->format('Y/m/d') : $date->diffForHumans()) ) }}
            </h3>
            <div class="box-tools pull-right">
                <span class='text-{{ $helperClass }}'>
                    <b>S/. {{ $dia->monto }}</b>
                </span>
                <!-- Buttons, labels, and many other things can be placed here! -->
                <!-- Here is a label for example -->
                <!--span class="label label-primary">Label</span-->
            </div>
        </div>
        <div class="box-body table-responsive no-padding">
            <table class="table table-hover">
                <tbody>
                    @foreach($dia->movimientos as $movimiento)
                    <?php
                        $helperClass = $movimiento->tipo === "ingreso" ? 'success' : 'danger';
                    ?>
                    <tr class='text-{{ $helperClass }} enlace' data-id='{{$movimiento->id}}'>
                        <td style='width:1%; white-space:nowrap;'><i class='fa fa-{{ $movimiento->icon }}'></i></td>
                        <td>{{ $movimiento->descripcion }}</td>
                        <td class='text-right'>S/. {{ $movimiento->monto }}</td>
                    </tr>
                    @endforeach

                </tbody>
            </table>
        </div>
    </div>
    <!--div class="panel panel-default">
        <div class="panel-heading">
        </div>
        <table class='table table-hover'>
            <tbody>
            </tbody>
        </table>
    </div-->
    @endforeach
    <a href="{{ $dias->nextPageUrl() }}" id='next'></a>

    <!--div class="panel panel-default">
        <div class="panel-body">
            <span class='pull-right'>
                <b>Saldo Inicial: <span class='{{ $cartera->saldo_inicial < 0 ? "text-danger" : "text-success" }}'>S/. {{ $cartera->saldo_inicial }}</span></b>
            </span>
        </div>
    </div-->
    <a class='btn btn-primary btn-fab' href='{{ asset("cartera/". $cartera->id ."/movimiento" )}}'>
        <i class='fa fa-plus'></i>
    </a>
</div>
<div class="modal fade" id="mdl-share" tabindex="-1" role="dialog" aria-labelledby="mdl-share-label">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['class' => 'form-horizontal', 'id' => 'frm-share']) !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="mdl-share-label">Compartir</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" name="cartera-id" value="{{ $cartera->id }}">
                <div class="form-group">
                    <label for="txt-correo" class='col-sm-3 col-md-2 control-label'>Correo</label>
                    <div class="col-sm-10">
                        <input type="email" class='form-control' name="correo" id='txt-correo'>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class='btn btn-primary pull-left'><i class='fa fa-check'></i> Listo</button>
                <button type="button" class='btn btn-default' data-dismiss='modal'><i class='fa fa-close'></i> Cancelar</button>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
</div>
@stop

@section('scripts')
<script type="text/javascript" src='{{asset("js/jquery.infinitescroll.min.js")}}'></script>
<script type="text/javascript">
    $(document).ready(function(){
        var selected_mov = -1;
        $('#frm-share').on('submit', function(e){
            e.preventDefault();
            $.ajax({
                url: "{{ asset('cartera/share') }}",
                type: 'post',
                dataType: 'json',
                processData: false,
                contentType: false,
                data: new FormData(this),
                success: function(response){
                    $('#mdl-share').modal('toggle');
                    location.reload();
                }
            });
        });
        $('#frm-new-movimiento').on('submit', function(e){
            e.preventDefault();
            $.ajax({
                url: selected_mov == -1 ? "{{ asset('movimiento/save') }}" : "{{ asset('movimiento/update') }}/" + selected_mov,
                type: 'post',
                dataType: 'json',
                processData: false,
                contentType: false,
                data: new FormData(this),
                success: function(response){
                    location.reload();
                }
            });
        });
        $('tr').on('click', function(e){
            selected_mov = $(this).data('id');
            location.href = "{{ asset('movimiento') }}/" + selected_mov;
        });
        $('#mdl-new-mov').on('hidden.bs.modal', function(e){
            document.getElementById('frm-new-movimiento').reset();
            selected_mov = -1;
        });
    });
</script>
<script type="text/javascript">
    $('#infinite-scroll').infinitescroll({
        navSelector  	: "#next:last",
        nextSelector 	: "a#next:last",
        itemSelector 	: "#infinite-scroll div.box",
        debug		 	: true,
        dataType	 	: 'html',
        donetext        : 'No hay más movimientos'
        //maxPage         : 3,
        //		prefill			: true,
        //		path: ["http://nuvique/infinite-scroll/test/index", ".html"]

        // behavior		: 'twitter',
        // appendCallback	: false, // USE FOR PREPENDING
        // pathParse     	: function( pathStr, nextPage ){ return pathStr.replace('2', nextPage ); }
    });
</script>
@stop
