@extends('main')
@section('title', 'Página Principal')
@section('header-1', 'Título')
@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Tus cuentas</h3>
                    <div class="box-tools pull-right">
                        <a href="{{ asset('cuenta/nueva')}}" class="btn btn-primary btn-sm"> <i class="fa fa-plus"></i> </a>
                    </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
                <div class="box-body no-padding">
                    <table class="table table-condensed table-hover">
                        <tbody id='lst-cuentas'>

                        </tbody>
                    </table>
                </div><!-- /.box-body -->
                <!-- <div class="box-footer">
                    The footer of the box
                </div>< box-footer > -->
            </div><!-- box -->
        </div>
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Presupuesto</h3>
                </div><!-- /.box-header -->
                <div class="box-body no-padding">
                    <table class="table table-condensed">
                        <tbody id='lst-presupuesto'>

                        </tbody>
                    </table>
                </div><!-- /.box-body -->
                <!-- <div class="box-footer">
                    The footer of the box
                </div>< box-footer > -->
            </div><!-- box -->
        </div>
        <!-- <div class="col-md-6">
            <div class="progress-group">
                <span class='progress-text'>Nombre categoría</span>
                <span class='progress-number'>10/100</span>
                <div class="progress-sm">
                    <div class="progress-bar progress-bar-aqua" style="width: 80%;"></div>
                </div>
            </div>
        </div> -->
    </div>
@endsection
@section('scripts')
<!-- <script src="{{ asset('js/auth.js') }}"></script>-->
<script src="{{ asset('js/web/cuentas.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function(){
        cuentas.loadCuentas();
    });
</script>
@endsection
