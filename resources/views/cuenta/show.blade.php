@extends('main')
@section('title', 'Página Principal')
@section('header-1', 'Título')
@section('content')
<div class="page-with-fab-btn" id='infinite-scroll'>
</div>
<a class='btn btn-primary btn-fab' href='{{ asset("cuenta/". $id_cuenta ."/movimiento" )}}'>
    <i class='fa fa-plus'></i>
</a>
<div class="modal fade" id="mdl-share" tabindex="-1" role="dialog" aria-labelledby="mdl-share-label">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['class' => 'form-horizontal', 'id' => 'frm-share']) !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="mdl-share-label">Compartir</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="txt-correo" class='col-sm-3 col-md-2 control-label'>Correo</label>
                    <div class="col-sm-10">
                        <input type="email" class='form-control' name="correo" id='txt-correo'>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class='btn btn-primary pull-left'><i class='fa fa-check'></i> Listo</button>
                <button type="button" class='btn btn-default' data-dismiss='modal'><i class='fa fa-close'></i> Cancelar</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection
@section('sidebar-content')
    <li>
        <a href="#" data-toggle="modal" data-target="#mdl-share" >
            <i class="menu-icon fa fa-share-alt bg-red"></i>

            <div class="menu-info">
                <h4 class="control-sidebar-subheading">Compartir</h4>
                <p>Agregar miembros a la cartera</p>
            </div>
        </a>
    </li>
    <li>
        <a href="#" id='btn-editar-cuenta'>
            <i class='menu-icon fa fa-pencil bg-red'></i>
            <div class="menu-info">
                <h4 class='control-sidebar-subheading'>Editar</h4>
                <p>
                    Modificar los datos de la cartera
                </p>
            </div>
        </a>
    </li>
@stop
@section('scripts')
<script src="{{ asset('js/web/movimientos.js')}}"></script>
<script src="{{ asset('js/web/cuentas.js')}}"></script>
<script type='text/javascript'>
    $(document).ready(function(){
        movimientos.idCuenta = '{{ $id_cuenta }}';
        cuentas.idCuenta = '{{ $id_cuenta }}';
        fw.main({
            load: movimientos.loadPage
        });
    });
</script>
<!-- <script src="{{ asset('js/auth.js') }}"></script> -->
@endsection
