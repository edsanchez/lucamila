@extends('main')
@section('title', 'Cuenta')
@section('header-1', 'Título')
@section('content')
<div>
    {!! Form::open(['class' => 'form-horizontal', 'id' => 'frm-cuenta']) !!}
    <div class="form-group">
        <label for="txt-nombre" class='control-label col-sm-3 col-md-2'>Nombre</label>
        <div class="col-sm-9 col-md-10">
            <input type="text" class='form-control' id='txt-nombre' name="nombre" value='' placeholder='P.e.: Ahorros'>
        </div>
    </div>
    <div class="form-group">
        <label for="txt-saldo" class='control-label col-sm-3 col-md-2'>Saldo Inicial</label>
        <div class="col-sm-5 col-md-4">
            <div class="input-group">
                <span class="input-group-addon">S/.</span>
                {!! Form::text('saldo-inicial', '', ["id" => "txt-saldo-inicial", "class" => 'form-control']) !!}
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-9 col-md-10 col-sm-offset-3 col-md-offset-2">
            <button type="submit" class='btn btn-primary'><i class='fa fa-check'></i> Listo</button>
            <button type="button" class='btn btn-default' id='btn-cancelar'><i class='fa fa-ban'></i> Cancelar</button>
            <button type="button" class='btn btn-danger hide' id='btn-eliminar'><i class='fa fa-trash'></i> Eliminar</button>
        </div>
    </div>
</div>
@stop

@section('scripts')
<script src="{{ asset('js/web/cuentas.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function(){
        cuentas.idCuenta = '{{ $id_cuenta or (-1) }}';
        cuentas.loadPage();
    });
</script>
@stop
