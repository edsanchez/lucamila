@extends('main')
@section('title', 'Página Principal')
@section('header-1', 'Título')
@section('content')
{!! Form::open(['class' => 'form-horizontal', 'id' => 'frm-movimiento']) !!}
<input type="hidden" name="id" id='txt-id' value="">
<div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="txt-monto" class='col-sm-3 col-md-4 control-label'>Monto</label>
                <div class="col-sm-9 col-md-8">
                    <div class="input-group">
                        <span class="input-group-addon">S/.</span>
                        {!! Form::text('monto', '', ["id" => "txt-monto", "class" => 'form-control']) !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class='form-group'>
                <label for="txt-fecha" class='col-sm-3 col-md-4 control-label'>Fecha</label>
                <div class="col-sm-9 col-md-8">
                    {!! Form::text('fecha', '', ['class' => 'form-control', 'id' => 'txt-fecha']) !!}
                </div>
            </div>
        </div>
    </div>
</div>
<div class='form-group'>
    <label for="chb-transferencia" class='col-sm-3 col-md-2 control-label'>Transferencia</label>
    <div class="col-sm-9 col-md-10">
        <input type="checkbox" name="chb-transferencia"> Sí, mover dinero a otra de mis cuentas
    </div>
</div>
<div class="" id='div-categorias'>
    <div class="form-group">
        <label for="rb-tipo" class='col-sm-3 col-md-2 control-label'>Tipo</label>
        <div class="col-sm-9 col-md-10">
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a id='tab-ingreso' href="#ingresos" aria-controls="home" role="tab" data-toggle="tab">Ingreso</a></li>
                <li role="presentation"><a id='tab-egreso' href="#egresos" aria-controls="profile" role="tab" data-toggle="tab">Gasto</a></li>
            </ul>
            <div class="tab-content" style='background: white;border: 1px solid #ddd;border-top-color: transparent;'>
                <div role="tabpanel" class="tab-pane active" id="ingresos">
                    <br>

                </div>
                <div role="tabpanel" class="tab-pane egreso" id="egresos">
                    <br>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="hide" id='div-transferencia'>
    <div class="form-group">
        <label for="cbo-destino" class='col-sm-3 col-md-2 control-label'>Destino</label>
        <div class="col-sm-3 col-md-4">
            <select class="form-control" name="destino" id='cbo-destino'>
            </select>
        </div>
    </div>
</div>
<div class="form-group">
    <label for="txt-descripcion" class='col-sm-3 col-md-2 control-label'>Descripción</label>
    <div class="col-sm-9 col-md-10">
        {!! Form::text('descripcion', '', ['class' => 'form-control', 'id' => 'txt-descripcion']) !!}
    </div>
</div>
<div class="form-group">
    <div class="col-sm-9 col-md-10 col-sm-offset-3 col-md-offset-2">
        <button type="submit" class='btn btn-primary'><i class='fa fa-check'></i> Listo</button>
        <button type="button" class='btn btn-default' id='btn-cancelar'><i class='fa fa-ban'></i> Cancelar</button>
    </div>
</div>
{!! Form::close() !!}
@endsection
@section('scripts')
<script src="{{ asset('js/web/movimientos.js')}}"></script>
<script type='text/javascript'>
    $(document).ready(function(){

        movimientos.idCuenta = '{{ $id_cuenta or "-1" }}';
        movimientos.idMovimiento = '{{ $id_movimiento or "-1" }}';
        movimientos.loadNewMovimiento();
    });
</script>
<!-- <script src="{{ asset('js/auth.js') }}"></script> -->
@endsection
