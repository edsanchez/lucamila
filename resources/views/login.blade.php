<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Camila | Ingreso</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css')}}">
        <link rel="stylesheet" href="{{ asset('css/AdminLTE.min.css')}}">
        <link rel="stylesheet" href="{{ asset('css/skins/skin-blue-light.min.css')}}">
        <link rel="stylesheet" href="{{ asset('css/lucamila.css')}}">
    </head>
    <body class='hold-transition login-page'>
        <div class="login-box">
            <div class="login-logo">
                <b>Lucamila</b>
            </div>
            <div class="login-box-body">
                <div class="overlay">
                  <i class="fa fa-refresh fa-spin"></i>
                </div>
                <p class="login-box-msg">Ingresa tus datos para iniciar sesión</p>
                {!! Form::open(['id' => 'frm-login']) !!}
                <div class="form-group has-feedback">
                    {!!
                        Form::email('correo', null, [
                        "class"         => "form-control",
                        "id"            => "txt-correo",
                        'placeholder'   => "ejemplo@correo.com"])
                        !!}
                    <span class="fa fa-envelope form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    {!!
                        Form::password('password', [
                        "class"         => "form-control",
                        "id"            => "txt-password",
                        "placeholder"   => "Contraseña"
                        ])
                    !!}
                    <span class="fa fa-lock form-control-feedback"></span>
                </div>
                <div id="messages">

                </div>
                {!! Form::submit('Ingresar', ["class" => "btn btn-primary btn-block btn-flat"]) !!}
                {!! Form::close() !!}
<!--
                <div class="social-auth-links text-center">
                <p>- O -</p>
                <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Ingresar usando
                Facebook</a>
                <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Ingresar usando
                Google+</a>
                </div>

                <a href="#">Olvidé mi contraseña</a><br> -->
            </div>
        </div>
        <script type="text/javascript" src='{{ asset("js/jquery-2.1.4.min.js") }}'></script>
        <script type="text/javascript" src='{{ asset("js/bootstrap.min.js") }}'></script>
        <script src="{{ asset('js/app.min.js')}}"></script>
        <script src="{{ asset('js/fw.js') }}"></script>
        <script src="{{ asset('js/web/usuarios.js') }}"></script>
    </body>
</html>
